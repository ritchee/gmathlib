#ifndef _GMATH_
#define _GMATH_

#include "def.h"

/*
#define lerp(n0, n1, n2, t) _lerp(n0, n1, n2, t)
#define bilerp(n0, n00, n01, n10, n11, tx, ty) do{\
	fp_t n1; _lerp(n1, n00, n01, tx);\
	fp_t n2; _lerp(n2, n10, n11, tx);\
	_lerp(n0, n1, n2, ty);\
}while(0)
#define trilerp(n0, n000, n001, n010, n011, \
					n100, n101, n110, n111, \
				tx, ty, tz) do{\
	fp_t n1, n2, n3, n4; \
	_lerp(n1, n000, n001, tx);\
	_lerp(n2, n010, n011, tx);\
	_lerp(n3, n1, n2, ty);\
	_lerp(n1, n100, n101, tx);\
	_lerp(n2, n110, n111, tx);\
	_lerp(n4, n1, n2, ty);\
	_lerp(n0, n3, n4, tz);\
}while(0)


#define _lerp(n0, n1, n2, t) n0 = (1-t)*n1 + t*n2
void catmull_rom_spln(_tp n0, _tp n1, _tp n2, _tp n3, _tp n4, fp_t t);
*/

fp64_t integral(fp64_t (*f)(fp64_t), int lo, int hi, int p); /* using Simpson's rule */
fp64_t deriv(fp64_t (*f)(fp64_t), fp64_t t);
fp64_t deriv2(fp64_t (*f)(fp64_t), fp64_t t);
fp64_t deriv3(fp64_t (*f)(fp64_t), fp64_t t);
fp64_t deriv4(fp64_t (*f)(fp64_t), fp64_t t);
fp64_t derivn(fp64_t (*f)(fp64_t), fp64_t t, int p);
ulong nchk(ulong n, ulong k);

#endif /* _GMATH_ */
