#ifndef _MATRIX_
#define _MATRIX_

#include "def.h"
#include "vector.h"

typedef fp_t mat2x2_t[2][2];
typedef fp_t mat3x3_t[3][3];
typedef fp_t mat4x4_t[4][4];

typedef mat2x2_t mat2;
typedef mat3x3_t mat3;
typedef mat4x4_t mat4;

#define addm3(m0, m1, m2) _madd3(m0, m1, m2)
#define addm4(m0, m1, m2) _madd4(m0, m1, m2)
#define subm3(m0, m1, m2) _msub3(m0, m1, m2)
#define subm4(m0, m1, m2) _msub4(m0, m1, m2)
#define mulmm3(m0, m1, m2) _mmmul3(m0, m1, m2)
#define mulmm4(m0, m1, m2) _mmmul4(m0, m1, m2)
#define mulmv3(v0, m1, v1) _mvmul3(v0, m1, v1)
#define mulmv4(v0, m1, v1) _mvmul4(v0, m1, v1)
#define mulvm3(v0, v1, m1) _vmmul3(v0, v1, m1)
#define mulvm4(v0, v1, m1) _vmmul4(v0, v1, m1)
#define mulmn3(m0, m1, n) _mnmul3(m0, m1, n)
#define mulmn4(m0, m1, n) _mnmul4(m0, m1, n)
#define trspm3(m0, m1) _mtrsp3(m0, m1)
#define trspm4(m0, m1) _mtrsp4(m0, m1)
#define trsp2m3(m0) _mtrsp23(m0)
#define trsp2m4(m0) _mtrsp24(m0)
#define idnm3(m0) _midn(m0, 3)
#define idnm4(m0) _midn(m0, 4)
#define sclmu _msclu
#define sclmn _mscln
#define trnsm _mtrns
#define rotm _mrot
#define rflm _mrfl
#define rflml _mrfll
#define shrm _mshr
#define orthm _morth
#define orthm2 _morth2
#define oblgm _moblg
#define prspm _mprsp
#define prspm2 _mprsp2
#define lkatm _mlkat
#define lkdirm _mlkdir
#define camm _mcam
#define invm3 _minv3
#define invm4 _minv4

#define cpym2(m0, m1) cpy(m0, m1, _sz(mat2))
#define cpym3(m0, m1) cpy(m0, m1, _sz(mat3))
#define cpym4(m0, m1) cpy(m0, m1, _sz(mat4))

#define MAT3_IDN ((mat3){{1.0f, 0.0f, 0.0f}, \
						{0.0f, 1.0f, 0.0f}, \
						{0.0f, 0.0f, 1.0f}})

#define MAT4_IDN ((mat4){{1.0f, 0.0f, 0.0f, 0.0f}, \
						{0.0f, 1.0f, 0.0f, 0.0f}, \
						{0.0f, 0.0f, 1.0f, 0.0f}, \
						{0.0f, 0.0f, 0.0f, 1.0f}})

#define MAT3_INIT {{1.0f, 0.0f, 0.0f}, \
					 {0.0f, 1.0f, 0.0f}, \
					 {0.0f, 0.0f, 1.0f}}
 
#define MAT4_INIT {{1.0f, 0.0f, 0.0f, 0.0f}, \
					 {0.0f, 1.0f, 0.0f, 0.0f}, \
					 {0.0f, 0.0f, 1.0f, 0.0f}, \
					 {0.0f, 0.0f, 0.0f, 1.0f}}

#define MATRIX_IMPLEMENTATION(dim)\
\
static inline void _madd##dim(mat##dim m0, mat##dim m1, mat##dim m2);\
static inline void _msub##dim(mat##dim m0, mat##dim m1, mat##dim m2);\
static inline void _mmmul##dim(mat##dim m0, mat##dim m1, mat##dim m2);\
static inline void _mvmul##dim(vec##dim v0, mat##dim m1, vec##dim v1);\
static inline void _vmmul##dim(vec##dim v0, vec##dim v1, mat##dim m1);\
static inline void _mnmul##dim(mat##dim m0, mat##dim m1, fp_t n);\
static inline void _mtrsp##dim(mat##dim m0, mat##dim m1);\
static inline void _mtrsp2##dim(mat##dim m0);\
\
void _madd##dim(mat##dim m0, mat##dim m1, mat##dim m2){\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			m0[i][j] = m1[i][j] + m2[i][j];\
}\
\
void _msub##dim(mat##dim m0, mat##dim m1, mat##dim m2){\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			m0[i][j] = m1[i][j] - m2[i][j];\
}\
\
void _mmmul##dim(mat##dim m0, mat##dim m1, mat##dim m2){\
	mat##dim m = {};\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			for(int k = 0; k < dim; ++k)\
				m[i][j] += m1[i][k]*m2[k][j];\
	cpym##dim(m0, m);\
}\
\
void _mvmul##dim(vec##dim v0, mat##dim m1, vec##dim v1){\
	vec##dim v = {};\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			v[i] += m1[i][j]*v1[j];\
	cpyv##dim(v0, v);\
}\
\
void _vmmul##dim(vec##dim v0, vec##dim v1, mat##dim m1){\
	vec##dim v = {};\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			v[i] += m1[j][i]*v1[j];\
	cpyv##dim(v0, v);\
}\
\
void _mnmul##dim(mat##dim m0, mat##dim m1, fp_t n){\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			m0[i][j] = m1[i][j]*(n);\
}\
\
void _mtrsp##dim(mat##dim m0, mat##dim m1){\
	mat##dim m = {};\
	for(int i = 0; i < dim; ++i)\
		for(int j = 0; j < dim; ++j)\
			m[i][j] = (i^j)?m1[j][i]:m1[i][j];\
	cpym##dim(m0, m);\
}\
	\
void _mtrsp2##dim(mat##dim m0){\
	fp_t n;\
	for(int i = 0; i < dim - 1; ++i)\
		for(int j = i + 1; j < dim; ++j){\
			n = m0[i][j];\
			m0[i][j] = m0[j][i];\
			m0[j][i] = n;\
		}\
}\

MATRIX_IMPLEMENTATION(3)
MATRIX_IMPLEMENTATION(4)

static inline void _midn(void* m0, int dim);
static inline void mat3to4(mat4 m0, mat3 m1);
static inline void mat4to3(mat3 m0, mat4 m1);

void _midn(void* m0, int dim){
	mset(m0, 0, dim*dim*_sz(fp_t));
	
	for(int i = 0; i < dim; ++i)
		*((fp_t*)m0 + i*dim + i) = 1.0f;
}

void mat3to4(mat4 m0, mat3 m1){
	for(int i = 0; i < 3; ++i)
		for(int j = 0; j < 3; ++j)
			m0[i][j] = m1[i][j];
}

void mat4to3(mat3 m0, mat4 m1){
	for(int i = 0; i < 3; ++i)
		for(int j = 0; j < 3; ++j)
			m0[i][j] = m1[i][j];
}

void _msclu(mat4 m0, vec3 nv, vec3 o, mat4 om);
void _mscln(mat4 m0, vec3 nv, vec3 u, vec3 o, mat4 om);
void _mtrns(mat4 m0, vec3 nv, mat4 om);
void _mrot(mat4 m0, fp64_t ang, vec3 u, mat4 om);
void _mrfl(mat4 m0, vec3 u, vec3 o, mat4 om);
void _mrfll(mat4 m0, vec3 u, vec3 o, mat4 om);
void _mshr(mat4 m0, fp64_t ang, vec3 u, vec3 o, vec3 w, mat4 om);
void _morth(mat4 m0, vec3 u, vec3 o, mat4 om);
void _morth2(mat4 m0, fp_t n, fp_t f, fp_t r, fp_t l, fp_t t, fp_t b, mat4 om);
void _moblg(mat4 m0, vec3 u, vec3 o, vec3 w, mat4 om);
void _mprsp(mat4 m0, fp64_t fovy, fp_t asp, fp_t n, fp_t f, mat4 om);
void _mprsp2(mat4 m0, fp_t n, fp_t f, fp_t r, fp_t l, fp_t t, fp_t b, mat4 om);
void _mlkat(mat4 m0, vec3 eye, vec3 la, vec3 up, mat4 om);
void _mlkdir(mat4 m0, vec3 eye, vec3 dir, vec3 up, mat4 om);
void _mcam(mat4 m0, const void* c, mat4 om);

fp_t _minv3(mat3 m0, mat3 m1);
fp_t _minv4(mat4 m0, mat4 m1);

#endif /* _MATRIX_ */

