#ifndef _VECTOR_
#define _VECTOR_

#include "def.h"

typedef fp_t* vectorp_t;
typedef fp_t vector2_t[2];
typedef fp_t vector3_t[3];
typedef fp_t vector4_t[4];

typedef vectorp_t vecp;
typedef vector2_t vec2;
typedef vector3_t vec3;
typedef vector4_t vec4;

#define X_INIT {1.0f, 0.0f, 0.0f}
#define Y_INIT {0.0f, 1.0f, 0.0f}
#define Z_INIT {0.0f, 0.0f, 1.0f}
#define O_INIT {0.0f, 0.0f, 0.0f}
#define U_INIT(n) {n, n, n}

#define X_AX ((vec3){1.0f, 0.0f, 0.0f})
#define Y_AX ((vec3){0.0f, 1.0f, 0.0f})
#define Z_AX ((vec3){0.0f, 0.0f, 1.0f})
#define O_VC ((vec3){0.0f, 0.0f, 0.0f})
#define U_VC(n) ((vec3){n, n, n})

#define addv2(v0, v1, v2) _vadd(v0, v1, v2, 2)
#define addv3(v0, v1, v2) _vadd(v0, v1, v2, 3)
#define addv4(v0, v1, v2) _vadd(v0, v1, v2, 4)

#define subv2(v0, v1, v2) _vsub(v0, v1, v2, 2)
#define subv3(v0, v1, v2) _vsub(v0, v1, v2, 3)
#define subv4(v0, v1, v2) _vsub(v0, v1, v2, 4)

#define mulv2(v0, v1, n) _vmul(v0, v1, n, 2)
#define mulv3(v0, v1, n) _vmul(v0, v1, n, 3)
#define mulv4(v0, v1, n) _vmul(v0, v1, n, 4)

#define sclv2(v0, v1, n) _vscl(v0, v1, n, 2)
#define sclv3(v0, v1, n) _vscl(v0, v1, n, 3)
#define sclv4(v0, v1, n) _vscl(v0, v1, n, 4)

#define tnsv2(m0, v1, v2) _vtns(m0, v1, v2, 2)
#define tnsv3(m0, v1, v2) _vtns(m0, v1, v2, 3)
#define tnsv4(m0, v1, v2) _vtns(m0, v1, v2, 4)

#define crsv2 _vcrs2
#define crsv3 _vcrs3

#define dotv2(v1, v2) ((v1)[0]*(v2)[0] + (v1)[1]*(v2)[1])
#define dotv3(v1, v2) ((v1)[0]*(v2)[0] + (v1)[1]*(v2)[1] + (v1)[2]*(v2)[2])
#define dotv4(v1, v2) ((v1)[0]*(v2)[0] + (v1)[1]*(v2)[1] + (v1)[2]*(v2)[2] + (v1)[3]*(v2)[3])

#define dotvn2(v0, n) _vdot(v0, (vec2){n, n}, 2)
#define dotvn3(v0, n) _vdot(v1, (vec3){n, n, n}, 3)
#define dotvn4(v0, n) _vdot(v1, (vec4){n, n, n, n}, 4)

#define magv2_sqr(v0) mag_sqr(v0, 2)
#define magv3_sqr(v0) mag_sqr(v0, 3)
#define magv2(v0) mag(v0, 2)
#define magv3(v0) mag(v0, 3)

#define mag_sqr(v0, dim) _vdot(v0, v0, dim)
#define mag(v0, dim) sqrt(_vdot(v0, v0, dim))

#define nrmv2(v0, v1) _vnrm(v0, v1, 2)
#define nrmv3(v0, v1) _vnrm(v0, v1, 3)

#define negv2(v0, v1) _vneg(v0, v1, 2)
#define negv3(v0, v1) _vneg(v0, v1, 3)
#define negv4(v0, v1) _vneg(v0, v1, 4)

#define projv2(v0, v1, v2) _vproj(v0, v1, v2, 2)
#define projv3 _vproj3
#define projv4(v0, v1, v2) _vproj(v0, v1, v2, 4)

#define v2to3(v3, v2) cpy(&(v3), &(v2), _sz(v2))
#define v3to2(v2, v3) cpy(&(v2), &(v3), _sz(v2))
#define v3to4(v4, v3) cpy(&(v4), &(v3), _sz(v3))
#define v4to3(v3, v4) cpy(&(v3), &(v4), _sz(v3))

#define cpyv2(v0, v1) cpy(v0, v1, _sz(vec2))
#define cpyv3(v0, v1) cpy(v0, v1, _sz(vec3))
#define cpyv4(v0, v1) cpy(v0, v1, _sz(vec4))

/*
#define _vadd(v0, v1, v2, dim) \
	for(int i = 0; i < (dim); ++i) \
		v0[i] = v1[i] + v2[i]

#define _vsub(v0, v1, v2, dim) \
	for(int i = 0; i < (dim); ++i) \
		v0[i] = v1[i] - v2[i]

#define _vmul(v0, v1, v2, dim) \
	for(int i = 0; i < dim; ++i) \
		v0[i] = v1[i]*v2[i]

#define _vscl(v0, v1, n, dim) \
	for(int i = 0; i < (dim); ++i) \
		v0[i] = v1[i]*(n)

#define _vtns(m0, v1, v2, dim) do{ \
	_midn(m0, dim); \
	for(int i = 0; i < dim; ++i) \
		for(int j = 0; j < dim; ++j) \
			m0[i][j] = v1[i]*v2[j]; \
}while(0)
	
#define _vcrs2(v1, v2) (v1[0]*v2[1] - v1[1]*v2[0])
	
#define _vcrs3(v0, v1, v2) do{ \
	v0[0] = v1[1]*v2[2] - v1[2]*v2[1]; \
	v0[1] = v1[2]*v2[0] - v1[0]*v2[2]; \
	v0[2] = v1[0]*v2[1] - v1[1]*v2[0]; \
}while(0)

#define _vdot2(v1, v2) (v1[0]*v2[0] + v1[1]*v2[1])
#define _vdot3(v1, v2) (v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2])
#define _vdot4(v1, v2) (v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2] + v1[3]*v2[3])

#define _vnrm(v0, v1, dim) do{ \
	fp_t __n = 1/mag(v1, dim); \
	_vscl(v0, v1, __n, dim); \
}while(0)

#define _vneg(v0, v1, n) \
	for(int i = 0; i < n; ++i) \
		v0[i] = -v1[i]

#define _vproj3(v0, v1, v2) do{ \
	fp_t __n = dotv3(v2, v1), d = dotv3(v2, v2); \
	__n /= d; \
	_vscl(v0, v2, __n, 3); \
}while(0)
*/


static inline void _vadd(vecp v0, vecp v1, vecp v2, int dim);
static inline void _vsub(vecp v0, vecp v1, vecp v2, int dim);
static inline void _vmul(vecp v0, vecp v1, vecp v2, int dim);
static inline void _vscl(vecp v0, vecp v1, fp_t n, int dim);
static inline void _vtns(void* m0, vecp v1, vecp v2, int dim);
static inline fp_t _vdot(vecp v1, vecp v2, int dim);
static inline void _vnrm(vecp v0, vecp v1, int dim);
static inline void _vneg(vecp v0, vecp v1, int dim);
static inline void _vproj(vecp v0, vecp v1, vecp v2, int dim);
static inline void _vproj3(vec3 v0, vec3 v1, vec3 v2);
static inline fp_t _vcrs2(vec2 v1, vec2 v2);
static inline void _vcrs3(vec3 v0, vec3 v1, vec3 v2);

void _vadd(vecp v0, vecp v1, vecp v2, int dim){
	while(dim--)
		v0[dim] = v1[dim] + v2[dim];
}

void _vsub(vecp v0, vecp v1, vecp v2, int dim){
	while(dim--)
		v0[dim] = v1[dim] - v2[dim];
}

void _vmul(vecp v0, vecp v1, vecp v2, int dim){
	while(dim--)
		v0[dim] = v1[dim] * v2[dim];
}

void _vscl(vecp v0, vecp v1, fp_t n, int dim){
	while(dim--)
		v0[dim] = v1[dim] * n;
}

void _vtns(void* m0, vecp v1, vecp v2, int dim){
	fp_t* m;
	alloc(m, dim*dim*_sz(fp_t));

	for(int i = 0; i < dim; ++i)
		for(int j = 0; j < dim; ++j)
			m[i*dim + j] = v1[i]*v2[j];

	cpy(m0, m, dim*dim*_sz(fp_t));
	free(m);
}

fp_t _vdot(vecp v1, vecp v2, int dim){
	fp_t s = 0;
	while(dim--)
		s += v1[dim]*v2[dim];
	return s;
}

void _vnrm(vecp v0, vecp v1, int dim){
	fp_t n = 1/mag(v1, dim);
	_vscl(v0, v1, n, dim);
}

void _vneg(vecp v0, vecp v1, int dim){
	while(dim--)
		v0[dim] = -v1[dim];
}

void _vproj(vecp v0, vecp v1, vecp v2, int dim){
	_vscl(v0, v2, 
		_vdot(v2, v1, dim)/_vdot(v2, v2, dim)
		, dim);
}

void _vproj3(vec3 v0, vec3 v1, vec3 v2){
	fp_t n = dotv3(v2, v1), d = dotv3(v2, v2);
	n /= d;
	_vscl(v0, v2, n, 3);
}

fp_t _vcrs2(vec2 v1, vec2 v2){
	return v1[0]*v2[1] - v1[1]*v2[0];
}

void _vcrs3(vec3 v0, vec3 v1, vec3 v2){
	vec3 v;

	v[0] = v1[1]*v2[2] - v1[2]*v2[1];
	v[1] = v1[2]*v2[0] - v1[0]*v2[2];
	v[2] = v1[0]*v2[1] - v1[1]*v2[0];
	
	cpyv3(v0, v);
}

#endif /* _VECTOR_ */

