#ifndef _UTIL_
#define _UTIL_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "def.h"

#define alloc(ptr, sz) do{ \
	if((ptr = malloc(sz)) == NULL) \
		fprintf(stderr, "fatal: out of memory!\n"); \
	exit(1); \
}while(0)

#define mfree(ptr) do{\
	free(ptr);\
	ptr = NULL;\
}while(0)

#define zero(ptr) memset(ptr, 0, _sz(*ptr))
#define mset(ptr, n, sz) memset(ptr, n, sz)

#define cpyd(p0, p1) _cpy(p0, p1, _sz(*p0))
#define cpys(p0, p1) _cpy(p0, p1, _sz(*p1))
#define cpy(p0, p1, sz) _cpy(p0, p1, sz)
#define _cpy(p0, p1, sz) memmove(p0, p1, sz)

#define _sz(tp) sizeof(tp)

#define min(a, b) ((a) > (b))?(b):(a)
#define max(a, b) ((a) < (b))?(b):(a)

#define lp(i, s) _loop(int, i, 0, s, _cmp_g, _pre_inc)
#define _cmp_g(a, b) (a) < (b)
#define _pre_inc(i) ++i
#define _post_inc(i) i++
#define _loop(t, i, v, l, cmp, trm) for(t i = v; cmp(i, l); trm(i))


#define randi(l, h) (rand()%((h) - (l) + 1) + (l))
#define randf(l, h) (((fp_t)rand()/RAND_MAX)*((h) - (l)) + (l))

#define randin(n) randi(0, n)
#define randfn(n) randf(0, n)

#define errlog(msg) do{\
	fprintf(stderr, "error: " msg "!\n");\
	exit(EXIT_FAILURE);\
}while(0)


#endif /* _UTIL_ */
