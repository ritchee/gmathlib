#ifndef _CAMERA_
#define _CAMERA_

typedef struct _camera{
	vec3 u,v,w;
	vec3 e;
	vec3 la;
}camera;

void cam_init(camera* c, vec3 la, vec3 eye, fp64_t rng, vec3 up);

#endif /* _CAMERA_ */