#ifndef _GEOMETRY_
#define _GEOMETRY_

#include "vector.h"

typedef vec2 pnt2;
typedef vec3 pnt3;

typedef struct {
	pnt3 p, q;
} line_seg;

typedef struct {
	vec2 n;
	fp_t d;
}line2;

typedef struct {
	pnt3 o;
	vec3 v;
} line, ray;

typedef struct {
	vec3 n;
	fp_t d;
} plane;

typedef struct {
	pnt2 c;
	fp_t r;
} circle;

typedef struct {
	pnt3 c;
	fp_t r;
} sphere;

typedef struct {
	pnt3 a,b,c;
} triangle;

typedef struct {
	pnt3 c;
	fp_t r[3];
} aabb;

//void qhull2();

void* cube_gen(fp_t len);
void* cube_gen2(fp_t len, pnt3 center, vec3 up);

static inline fp_t lerp(fp_t v1, fp_t v2, fp_t t);
static inline fp_t bilerp(fp_t v1, fp_t v2, fp_t v3, fp_t v4, fp_t t1, fp_t t2);
static inline fp_t trilerp(fp_t* v, fp_t* t);
static inline fp_t area2(pnt2 pt1, pnt2 pt2, pnt2 pt3);
static inline fp_t area3(pnt3 pt1, pnt3 pt2, pnt3 pt3);

static inline fp_t distance_ptl2(pnt2 pt1, line* ln1);
static inline fp_t distance_pts2(pnt2 pt1, pnt2 pt2, pnt2 pt3);
static inline fp_t distance_ptl3(pnt3 pt1, line* ln1);
static inline fp_t distance_ptp3(pnt3 pt1, plane* pln1);
static inline fp_t distance_ll3(line* ln1, line* ln2);

static inline int intersect_sphere(sphere* s1, sphere* s2);

static inline int intersect_aabb(aabb* bb1, aabb* bb2);
static inline void update_aabb(aabb* bb0, aabb* bb1, fp_t m[4][4]);

fp_t lerp(fp_t v1, fp_t v2, fp_t t) {
  	return v1 + t * (v2 - v1);
}

fp_t bilerp(fp_t v1, fp_t v2, fp_t v3, fp_t v4, fp_t t1, fp_t t2){
	return lerp(lerp(v1, v2, t1), lerp(v3, v4, t1), t2);
}

fp_t trilerp(fp_t* v, fp_t* t){
	return lerp(bilerp(v[0], v[1], v[2], v[3], t[0], t[1]), bilerp(v[4], v[5], v[6], v[7], t[0], t[1]), t[2]);
}

fp_t area2(pnt2 pt1, pnt2 pt2, pnt2 pt3){
	return ((pt2[0] - pt1[0])*(pt3[1] - pt1[1]) - (pt2[1] - pt1[1])*(pt3[0] - pt1[0]))/2.0f;
}

fp_t area3(pnt3 pt1, pnt3 pt2, pnt3 pt3){
	vec3 a = {pt2[0] - pt1[0], pt2[1] - pt1[1], pt2[2] - pt1[2]};
	vec3 b = {pt3[0] - pt1[0], pt3[1] - pt1[1], pt3[2] - pt1[2]};
	
	crsv3(a, a, b);
	
	return fabs(magv3(a))/2;
}

fp_t distance_ptl2(pnt2 pt, line* ln){
	vec2 ao;
	subv2(ao, pt, ln->o);
	
	return fabs(ao[0]*(ln->v[1]) - ao[1]*(ln->v[0]))/magv2(ln->v);
}

fp_t distance_pts2(pnt2 pt0, pnt2 pt1, pnt2 pt2){
	vec2 ab, ac;
	subv2(ac, pt0, pt1);
	subv2(ab, pt2, pt1);
	
	return fabs(ac[0]*ab[1] - ac[1]*ab[0])/magv2(ab);
}

fp_t distance_ptl3(pnt3 pt, line* ln){
	vec3 ao;
	subv3(ao, pt, ln->o);
	crsv3(ao, ao, ln->v);
	
	return magv3(ao)/magv3(ln->v);
}

fp_t distance_ptp3(pnt3 pt, plane* pln){
	return fabs(pln->d - dotv3(pln->n, pt))/magv3(pln->n);
}

fp_t distance_ll3(line* ln1, line* ln2){
	plane p;
	crsv3(p.n, ln1->v, ln2->v);
	p.d = dotv3(p.n, ln1->o);
	
	return distance_ptp3(ln2->o, &p);
}

int intersect_sphere(sphere* s1, sphere* s2){
	vec3 v;
	subv3(v, s1->c, s1->c);
	return magv3_sqr(v) <= sq(s1->r + s2->r);
}

int intersect_aabb(aabb* bb1, aabb* bb2){
	if (fabs(bb1->c[0] - bb2->c[0]) > (bb1->r[0] + bb2->r[0])) return 0;
	if (fabs(bb1->c[1] - bb2->c[1]) > (bb1->r[1] + bb2->r[1])) return 0;
	if (fabs(bb1->c[2] - bb2->c[2]) > (bb1->r[2] + bb2->r[2])) return 0;
	return 1;
}

void update_aabb(aabb* bb0, aabb* bb1, fp_t m[4][4]){
	for (int i = 0; i < 3; i++) {
		bb0->c[i] = m[i][3];
		bb0->r[i] = 0.0f;
		for (int j = 0; j < 3; j++) {
			bb0->c[i] += m[i][j] * bb1->c[j];
			bb0->r[i] += fabs(m[i][j]) * bb1->r[j];
		}
	}
}

#endif /* _GEOMETRY_ */
