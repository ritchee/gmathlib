#ifndef _UTIL_
#define _UTIL_

#include "def.h"
#include "container/common.h"

#define cpyd(p0, p1) _cpy(p0, p1, _sz(*p0))
#define cpys(p0, p1) _cpy(p0, p1, _sz(*p1))
#define cpy(p0, p1, sz) _cpy(p0, p1, sz)
#define _cpy mcpy

#define todeg(n) (n)*180.f/M_PI
#define torad(n) (n)*M_PI/180.f

#define cb(x) ((x)*(x)*(x))
#define sq(x) ((x)*(x))

#define clamp(x, l, h) ((x) = ((x) < (l))?(l):(((x) > (h))?(h):(x)))

#define fp_cmp(f1, f2) _fp_cmp(f1, f2, FP_PREC)

#ifdef DB_PREC
#define _fp_cmp(f1, f2, prec) (((fp64_t)(f1 - f2) <= prec) && ((fp64_t)(f1 - f2) >= -prec))
#define FP_PREC FP64_EPS
#else
#define _fp_cmp(f1, f2, prec) (((f1 - f2) <= prec) && ((f1 - f2) >= -prec))
#define FP_PREC FP32_EPS
#endif /* DB_PREC */

#define lg(msg) puts(#msg)
	
char* readtxtf(const char* s);
void fatal(const char* fmt, ...);
void swap(void* p1, void* p2, size_t n);

#endif /* _UTIL_ */

