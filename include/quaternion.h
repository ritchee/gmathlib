#ifndef _QUATERNION_
#define _QUATERNION_

#include "def.h"

typedef fp_t quaternion_t[4];
typedef quaternion_t quat;

#define addq add_vec4
#define subq sub_vec4
#define dotq dotv4
#define negq negv4
#define nrmq(q0) mag(q0, 4)
#define invq(q0, q1) _qinv(q0, q1)
#define cnjq(q0, q1) _conj(q0, q1)
#define sclq(q0, n) _vscl(q0, n, 4)
#define mulq(q0, q1, q2) _qmul(q0, q1, q2)
#define pwrq(q0, q1, e) _qpwr(q0, q1, e)
#define difq(q0, q1, q2) _qdif(q0, q1, q2)

#define rotq(v0, ang, u, v1) _qrot(v0, ang, u, v1)
#define qtom4(m0, ang, u) _qtom(m0, ang, u)
#define slerpq(q0, q1, q2, n) _qslerp(q0, q1, q2, n)

#define cpyq(q0, q1) cpy(q0, q1, _sz(quat))

/*
#define _conj(q0, q1) do{\
	q0[0] = -q1[0];\
	q0[1] = -q1[1];\
	q0[2] = -q1[2];\
	q0[3] =  q1[3];\
}while(0)
#define _qinv(q0, q1) do{\
	fp_t __n = mag_sqr(q1, 4);\
	q0[0] = -q1[0]/n;\
	q0[1] = -q1[1]/n;\
	q0[2] = -q1[2]/n;\
	q0[3] =  q1[3]/n;\
}while(0)
*/

void _qrot(vec3 v0, fp64_t ang, vec3 u, vec3 v1);
void _qtom(fp_t m0[4][4], fp64_t ang, vec3 u, mat4 om);
void _qslerp(quat q0, quat q1, quat q2, fp_t n);

static inline void _qmul(quat q0, quat q1, quat q2);
static inline void _conj(quat q0, quat q1);
static inline void _qinv(quat q0, quat q1);
static inline void _qpwr(quat q0, quat q1, fp_t e);
static inline void _qdif(quat q0, quat q1, quat q2);

void _qmul(quat q0, quat q1, quat q2){
	quat q;
	
	q[3] = q1[3]*q2[3] - (q1[0]*q2[0] + q1[1]*q2[1] + q1[2]*q2[2]);
	q[0] = q1[3]*q2[0] + q2[3]*q1[0] + (q1[1]*q2[2] - q1[2]*q2[1]);
	q[1] = q1[3]*q2[1] + q2[3]*q1[1] + (q1[2]*q2[0] - q1[0]*q2[2]);
	q[2] = q1[3]*q2[2] + q2[3]*q1[2] + (q1[0]*q2[1] - q1[1]*q2[0]);
	
	cpyq(q0, q);
}

void _conj(quat q0, quat q1){
	q0[0] = -q1[0];
	q0[1] = -q1[1];
	q0[2] = -q1[2];
	q0[3] =  q1[3];
}

void _qinv(quat q0, quat q1){
	fp_t n = mag_sqr(q1, 4);
	q0[0] = -q1[0]/n;
	q0[1] = -q1[1]/n;
	q0[2] = -q1[2]/n;
	q0[3] =  q1[3]/n;
}

void _qpwr(quat q0, quat q1, fp_t e){
	if(fabs(q1[3]) > 0.9999f) return;
	fp_t ang = acos(q1[3]);
	fp_t nang = ang * e;
	fp_t nsin = sin(nang) / sin(ang);
	
	q0[0] = q1[0] * nsin;
	q0[1] = q1[1] * nsin;
	q0[2] = q1[2] * nsin;
	q0[3] = cos(nang);
}

void _qdif(quat q0, quat q1, quat q2){
	quat q, _q1;
	
	_qinv(_q1, q1);
	_qmul(q, q2, _q1);
	
	cpyq(q0, q);
}

#endif /* _QUATERNION_ */
