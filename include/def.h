#ifndef _DEF_
#define _DEF_

typedef float fp32_t;
typedef double fp64_t;

#if defined(_WIN32) || defined(_WIN64)
typedef double ld_t;
#else
typedef long double ld_t;
#endif /* _WIN32 || _WIN64 */

#ifdef DB_PREC
typedef fp64_t fp_t;
#else
typedef fp32_t fp_t;
#endif /* DB_PRECISION */

#ifdef __cplusplus
typedef enum{false, true} bool;
#endif /* __cplusplus */

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ullong;

#ifndef PI
#define PI 3.141592
#endif /* PI */

#define E_NO  2.718282

#define FP32_EPS 1e-5
#define FP64_EPS 1e-9

#ifdef DB_PREC
#define EPS FP64_EPS
#else
#define EPS FP32_EPS
#endif /* DB_PRECISION */

#endif /* _DEF_ */
