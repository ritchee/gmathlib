#include <float.h>

#include "util.h"
#include "geometry.h"

#define x 0
#define y 1
#define z 2
#define q 3

/* utility functions */
/*
static int ffseg(pnt2 pt1, pnt2 pt2, pnt2* pv, int n){
	vec2 v, t, a;
	cpyv2(a, pt1);
	subv2(v, pt2, a);
	vec2 nm = {-v[y], v[x]};
	
	int rt = -1;
	fp_t max = FLT_MIN, rm = FLT_MIN;
	fp_t d,r;
	pnt2* p = pv;
	
	for(int i = 0; i < n; ++i){
		subv2(t, p[i], a);
		d = dotv2(t, nm);
		r = dotv2(t, v);
		if(d > max || (d == max && r > rm)){
			rt = i;
			max = d;
			rm = r;
		}
	}
	
	return rt;
}*/

/* geometry functions */
/*
fp_t distanceptl2(pnt2 pt1, line* ln1){
	vec2 ac;
	vec2 d;
	cpyv2(d, ln1->d);
	
	addv2(ac, pt1, ln1->o);
	
	return fabs(ac[x]*d[y] - ac[y]*d[x])/magv2(d);
}

fp_t distancepts2(pnt2 pt0, pnt2 pt1, pnt2 pt2){
	vec2 ab, ac;
	subv2(ac, pt0, pt1);
	subv2(ab, pt2, pt1);
	
	return fabs(ac[x]*ab[y] - ac[y]*ab[x])/magv2(ab);
}

fp_t distanceptl3(pnt3 pt1, line* ln1){
	vec3 ac;
	vec3 d;
	cpyv3(d, ln1->d);
	
	addv3(ac, pt1, ln1->o);
	crsv3(ac, ac, d);
	
	return magv3(ac)/magv3(d);
}

fp_t distanceptp3(pnt3 pt1, plane* pln1){
	return fabs(pln1->d - dotv3(pln1->n, pt1))/magv3(pln1->n);
}

fp_t distancell3(line* ln1, line* ln2){
	vec3 n;
	
	crsv3(n, ln1->d, ln2->d);
	plane p;
	cpyv3(p.n, n);
	p.d = dotv3(n, ln1->o);
	return dstnptp3(ln2->o, &p);
}
*/

void* cube_gen(fp_t len){
	len /= 2;
	
	vec3* cb;
	alloc(cb, _sz(vec3)*8);
	
	cb[0][0] =   len; cb[0][1] =   len; cb[0][2] =   len;
	cb[1][0] =   len; cb[1][1] =   len; cb[1][2] = -len;
	cb[2][0] = -len; cb[2][1] =   len; cb[2][2] = -len;
	cb[3][0] = -len; cb[3][1] =   len; cb[3][2] =   len;
	cb[4][0] =   len; cb[4][1] = -len; cb[4][2] =   len;
	cb[5][0] =   len; cb[5][1] = -len; cb[5][2] = -len;
	cb[6][0] = -len; cb[6][1] = -len; cb[6][2] = -len;
	cb[7][0] = -len; cb[7][1] = -len; cb[7][2] =   len;
	/*
	vec3 vert[] = {{len, len, len},
			 {len, len, -len},
			 {-len, len, -len},
			 {-len, len, len},
			 {len, -len, len},
			 {len, -len, -len},
			 {-len, -len, -len},
			 {-len, -len, len}};*/
			 
	return cb;
}

void* cube_gen2(fp_t len, pnt3 center, vec3 up){
	len /= 2;
	vec3 c, u, v, w;
	cpyv3(c, center);
	cpyv3(u, up);
	nrmv3(u, u);
	v[0] = u[1]; v[1] = -u[0]; v[2] = 0;
	crsv3(w, u, v);
	
	sclv3(u, u, len);
	sclv3(v, v, len);
	sclv3(w, w, len);
	
	vec3* cb;
	alloc(cb, _sz(vec3)*8);
	
	cb[0][0] = c[0] + u[0]+v[0]+w[0]; cb[0][1] = c[1] + u[1]+v[1]+w[1]; cb[0][2] = c[2] + u[2]+v[2]+w[2];
	cb[1][0] = c[0] + u[0]+v[0]-w[0]; cb[1][1] = c[1] + u[1]+v[1]-w[1]; cb[1][2] = c[2] + u[2]+v[2]-w[2];
	cb[2][0] = c[0] - u[0]+v[0]-w[0]; cb[2][1] = c[1] - u[1]+v[1]-w[1]; cb[2][2] = c[2] - u[2]+v[2]-w[2];
	cb[3][0] = c[0] - u[0]+v[0]+w[0]; cb[3][1] = c[1] - u[1]+v[1]+w[1]; cb[3][2] = c[2] - u[0]+v[0]+w[0];
	cb[4][0] = c[0] + u[0]-v[0]+w[0]; cb[4][1] = c[1] + u[1]-v[1]+w[1]; cb[4][2] = c[2] + u[2]-v[2]+w[2];
	cb[5][0] = c[0] + u[0]-v[0]-w[0]; cb[5][1] = c[1] + u[1]-v[1]-w[1]; cb[5][2] = c[2] + u[2]-v[2]-w[2];
	cb[6][0] = c[0] - (u[0]+v[0]+w[0]); cb[6][1] = c[1] - (u[1]+v[1]+w[1]); cb[6][2] = c[2] - (u[2]+v[2]+w[2]);
	cb[7][0] = c[0] - u[0]-v[0]+w[0]; cb[7][1] = c[1] - u[1]-v[1]+w[1]; cb[7][2] = c[2] - u[2]-v[2]+w[2];
	
	return cb;
}

#undef x
#undef y
#undef z
#undef q

