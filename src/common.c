#include <stdarg.h>

#include "util.h"

char* readtxtf(const char* s){
	FILE* file;
	int len;
	char* buf;

	if((file = fopen(s, "r")) == NULL)
		fatal("%s not found!\n", s);

	fseek(file, 0L, SEEK_END);
	len = ftell(file);

	buf = malloc(len);
	fseek(file, 0L, SEEK_SET);
	fread(buf, 1, len - 1, file);
	buf[len - 1] = '\0';
	fclose(file);

	return buf;
}

void fatal(const char* fmt, ...){
        va_list args;

        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);

        exit(1);
}

void swap(void* p1, void* p2, size_t n){
	uchar* a = p1;
	uchar* b = p2;
	register uchar t;
	
	while(n--){
		t = *a ^ *b;
		*b = t ^ *b;
		*a = t ^ *b;
	}
}
