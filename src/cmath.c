#include <stdio.h>

#include "util.h"
#include "cmath.h"
/*
void catmull_rom_spln(_tp n0, _tp n1, _tp n2, _tp n3, _tp n4, fp_t t){
	_tp d1, d2, d3, a1, a2, a3, a4;
	
	d1 = (n2 - n0)/2;
	d2 = (n3 - n1)/2;
	d3 = n2 - n1;
	
	a4 = d1 + d2 - 2*d3;
	a3 = 3*d3 - 2*d1 - d2;
	a2 = d1;
	a1 = n1;
	
	
	n0 = a3*cb(t) + a2*sq(t) + a1*t + a0;
}
*/
#include <stdlib.h>
ulong nchk(ulong n, ulong k){
	register ulong t = max(k, n-k);
	k = min(k, n-k);
	
	for(ulong i = n-1; i > t; --i)
		n *= i;
	while(k > 1)
		n /= k--;
	
	return n;
}

fp64_t integral(fp64_t (*f)(fp64_t), int lo, int up, int p){
	fp64_t h, s;
	
	h = (up-lo)/(fp64_t)p;
	s = f(lo) + f(up);
	for(int i = 1; i < p; ++i){
		s += 2*((i&1)+1)*f(lo + i*h);
	}
	s *= h/3;
	
	return s;
}

fp64_t deriv(fp64_t (*f)(fp64_t), fp64_t t){
	return (f(t+FP64_EPS)-f(t))/FP64_EPS;
}

/* the more complicated the function, the more precise return value */
fp64_t deriv2(fp64_t (*f)(fp64_t), fp64_t t){
	return (f(t +2.0f*FP32_EPS) - 2*f(t+FP32_EPS)+f(t))/(FP32_EPS*FP32_EPS);
}

/* the more complicated the function, the more precise return value */
fp64_t deriv3(fp64_t (*f)(fp64_t), fp64_t t){
#define __eps 5e-4 /* 9e-5 for small values */
	return (f(t+3*__eps)-3*f(t+2*__eps)+3*f(t+__eps)-f(t))/(__eps*__eps*__eps);
#undef __eps
}

#define __eps 4e-3
fp64_t deriv4(fp64_t (*f)(fp64_t), fp64_t t){
	return (f(t+4*__eps)-4*f(t+3*__eps)+6*f(t+2*__eps)-4*f(t+__eps)+f(t))/ \
			(__eps*__eps*__eps*__eps);
}

fp64_t derivn(fp64_t (*f)(fp64_t), fp64_t t, int p){

	fp64_t s = -1.0f;
	fp64_t n = f(t+p*__eps);
	printf("%lf\n", n);
	
	for(int i = 1; i < p; ++i){
		n += s*nchk(p, i)*f(t+(p-i)*__eps);
		s *= -1;
	}
	
	n += s*f(t);
	
	while(p--)
		n /= (__eps);
	
	return n;
}
#undef __eps
