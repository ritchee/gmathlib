#include <string.h>
#include <math.h>
#include <stdio.h>

#include "util.h"
#include "vector.h"
#include "matrix.h"
#include "quaternion.h"
#include "camera.h"

#define x 0
#define y 1
#define z 2
#define q 3

void cam_init(camera* c, vec3 la, vec3 e, fp64_t rng, vec3 up){
	vec3 _c[5];
	
	cpyv3(_c[3], e);
	cpyv3(_c[4], la);
	subv3(_c[0], _c[3], _c[4]);
	nrmv3(_c[0], _c[0]);
	
	if(rng != 0.0f){
		_c[4][0] = -_c[0][0]*rng + _c[3][0];
		_c[4][1] = -_c[0][1]*rng + _c[3][1];
		_c[4][2] = -_c[0][2]*rng + _c[3][2];
	}
	
	crsv3(_c[2], _c[0], up);
	nrmv3(_c[2], _c[2]);
	crsv3(_c[1], _c[2], _c[0]);
	
	cpy(c, _c, _sz(camera));
}

void _msclu(mat4 m0, vec3 nv, vec3 o, mat4 om){
	mat4 m = {	{nv[x], 0.0f, 0.0f, (1-nv[x])*o[x]},
				{0.0f, nv[x], 0.0f, (1-nv[y])*o[y]},
				{0.0f, 0.0f, nv[x], (1-nv[z])*o[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mscln(mat4 m0, vec3 nv, vec3 u, vec3 o, mat4 om){
	vec3 v;
	nrmv3(v, u);
	
	mat4 m = MAT4_INIT;
	mat3 mt = {	{sq(v[x]), v[x]*v[y], v[x]*v[z]},
				{v[y]*v[x], sq(v[y]), v[y]*v[z]},
				{v[z]*v[x], v[z]*v[y], sq(v[z])}};
	
	mat3 ms = {	{1-nv[x], 0.0f, 0.0f},
				{0.0f, 1-nv[y], 0.0f},
				{0.0f, 0.0f, 1-nv[z]}};
	
	mulmm3(mt, ms, mt);
	
	for(int i = 0; i < 3; ++i) mt[i][i] = 1.0f - mt[i][i];
	mat3to4(m, mt);
	
	fp64_t n = dotv3(v, o);
	sclv3(v, v, n);
	mulmv3(v, ms, v);
	for(int i = 0; i < 2; ++i) m[i][3] = v[i];
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mtrns(mat4 m0, vec3 nv, mat4 om){
	mat4 m = {	{1.0f, 0.0f, 0.0f, nv[x]},
				{0.0f, 1.0f, 0.0f, nv[y]},
				{0.0f, 0.0f, 1.0f, nv[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mrot(mat4 m0, fp64_t ang, vec3 u, mat4 om){
	vec3 v;
	nrmv3(v, u);
	fp64_t c = cos(ang), s = sin(ang);
	
	nrmv3(v, v);
	
	mat4 m = {	{c, -s*v[z], s*v[y], 0.0f},
				{s*v[z], c, -s*v[x], 0.0f},
				{-s*v[y], s*v[x], c, 0.0f},
				{0.0f, 0.0f, 0.0f, 1.0f}},
\
		mt = {	{sq(v[x]), v[x]*v[y], v[x]*v[z], 0.0f},
				{v[y]*v[x], sq(v[y]), v[y]*v[z], 0.0f},
				{v[z]*v[x], v[z]*v[y], sq(v[z]), 0.0f},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	mat4 mp;
	
	subm4(mp, m, MAT4_IDN);
	mulmm4(mp, mp, mt);
	subm4(m, m, mp);
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mrfl(mat4 m0, vec3 u, vec3 o, mat4 om){
	vec3 v;
	nrmv3(v, u);
	fp64_t n = 2*dotv3(v, o);
	
	mat4 m = {	{1-2*sq(v[x]), 0.0f - 2*v[x]*v[y], 0.0f - 2*v[x]*v[z], o[x] - n*v[x]},
				{0.0f - 2*v[y]*v[x], 1-2*sq(v[y]), 0.0f - 2*v[y]*v[z], o[y] - n*v[y]},
				{0.0f - 2*v[z]*v[x], 0.0f - 2*v[z]*v[y], 1-2*sq(v[z]), o[z] - n*v[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mrfll(mat4 m0, vec3 u, vec3 o, mat4 om){
	vec3 v;
	nrmv3(v, u);
	fp64_t n = 2*dotv3(v, o);
	
	mat4 m = {	{2*sq(v[x])-1, 2*v[x]*v[y], 2*v[x]*v[z], n*v[x] - o[x]},
				{2*v[y]*v[x], 2*sq(v[y])-1, 2*v[y]*v[z], n*v[y] - o[y]},
				{2*v[z]*v[x], 2*v[z]*v[y], 2*sq(v[z])-1, n*v[z] - o[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mshr(mat4 m0, fp64_t ang, vec3 u, vec3 o, vec3 w, mat4 om){
	vec3 v, r, p;
	nrmv3(v, u);
	cpyv3(r, w);
	cpyv3(p, o);
	fp64_t t = tan(ang);
	
	mat4 m = {	{1+t*v[x]*r[x], t*v[y]*r[x], t*v[z]*r[x], -dotv3(v,p)*r[x]},
				{t*v[x]*r[y], 1+t*v[y]*r[y], t*v[z]*r[y], -dotv3(v,p)*r[y]},
				{t*v[x]*r[z], t*v[y]*r[z], 1+t*v[z]*r[z], -dotv3(v,p)*r[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _morth(mat4 m0, vec3 u, vec3 o, mat4 om){
	vec3 v, p;
	nrmv3(v, u);
	cpyv3(p, o);
	
	mat4 m = {	{1.0f - sq(v[x]), 0.0f - v[x]*v[y], 0.0f - v[x]*v[z], dotv3(v,p)*v[x]},
				{0.0f - v[y]*v[x], 1.0f - sq(v[y]), 0.0f - v[y]*v[z], dotv3(v,p)*v[y]},
				{0.0f - v[z]*v[x], 0.0f - v[z]*v[y], 1.0f - sq(v[z]), dotv3(v,p)*v[z]},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _morth2(mat4 m0, fp_t n, fp_t f, fp_t r, fp_t l, fp_t t, fp_t b, mat4 om){
	mat4 m = {	{2/(r-l), 0.0f, 0.0f, (r+l)/(l-r)},
				{0.0f, 2/(t-b), 0.0f, (t+b)/(b-t)},
				{0.0f, 0.0f, 2/(n-f), (n+f)/(f-n)},
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _moblg(mat4 m0, vec3 u, vec3 o, vec3 w, mat4 om){
	vec3 v, r, p;
	nrmv3(v, u);
	cpyv3(r, w);
	cpyv3(p, o);
	
	mat4 m = {	{1.0f - (v[x]*r[x]/dotv3(v,r)), 0.0f - (v[y]*r[x]/dotv3(v,r)), \
					0.0f - (v[z]*r[x]/dotv3(v,r)), dotv3(v,p)*r[x]/dotv3(v,r)},
					
				{0.0f - (v[x]*r[y]/dotv3(v,r)), 1.0f - (v[y]*r[y]/dotv3(v,r)), \
					0.0f - (v[z]*r[y]/dotv3(v,r)), dotv3(v,p)*r[y]/dotv3(v,r)},
					
				{0.0f - (v[x]*r[z]/dotv3(v,r)), 0.0f - (v[y]*r[z]/dotv3(v,r)), \
					1.0f - (v[z]*r[z]/dotv3(v,r)), dotv3(v,p)*r[z]/dotv3(v,r)},
					
				{0.0f, 0.0f, 0.0f, 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mprsp(mat4 m0, fp64_t fovy, fp_t asp, fp_t n, fp_t f, mat4 om){
	fp_t t = 1/tan(fovy/2);
	
	mat4 m = {	{t/asp, 0.0f, 0.0f, 0.0f},
				{0.0f, t, 0.0f, 0.0f},
				{0.0f, 0.0f, (n+f)/(n-f), -1.0f},
				{0.0f, 0.0f, (2*n*f)/(n-f), 0.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mprsp2(mat4 m0, fp_t n, fp_t f, fp_t r, fp_t l, fp_t t, fp_t b, mat4 om){
	fp_t d1 = r-l;
	fp_t d2 = t-b;
	fp_t d3= n-f;
	
	mat4 m = {	{2*n/d1, 0.0f, (l+r)/d1, 0.0f}, 
				{0.0f, 2*n/d2, (t+b)/d2, 0.0f}, 
				{0.0f, 0.0f, (f+n)/d3, 2*f*n/d3}, 
				{0.0f, 0.0f, -1.0f, 0.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mlkat(mat4 m0, vec3 eye, vec3 la, vec3 up, mat4 om){
	vec3 u,v,w;
	vec3 l, e;
	
	cpyv3(l, la);
	cpyv3(e, eye);
	subv3(u, e, l);
	nrmv3(u, u);
	crsv3(v, up, u);
	nrmv3(v, v);
	crsv3(w, u, v);
	
	mat4 m = {	{v[x], w[x], u[x], 0.0f}, 
				{v[y], w[y], u[y], 0.0f}, 
				{v[z], w[z], u[z], 0.0f}, 
				{-dotv3(v, e), -dotv3(w, e), -dotv3(u, e), 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mlkdir(mat4 m0, vec3 eye, vec3 dir, vec3 up, mat4 om){
	vec3 u,v,w;
	vec3 e;
	
	cpyv3(e, eye);
	nrmv3(u, dir);
	crsv3(v, u, up);
	nrmv3(v, v);
	crsv3(w, v, u);
	
	mat4 m = {	{v[x], w[x], -u[x], 0.0f}, 
				{v[y], w[y], -u[y], 0.0f}, 
				{v[z], w[z], -u[z], 0.0f}, 
				{-dotv3(v, e), -dotv3(w, e), dotv3(u, e), 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

void _mcam(mat4 m0, const void* c, mat4 om){
	camera _c;
	cpy(&_c, c, _sz(camera));
	
	mat4 m = {	{_c.v[x], _c.w[x], _c.u[x], 0.0f}, 
				{_c.v[y], _c.w[y], _c.u[y], 0.0f}, 
				{_c.v[z], _c.w[z], _c.u[z], 0.0f}, 
				{-dotv3(_c.u, _c.e), -dotv3(_c.v, _c.e), -dotv3(_c.w, _c.e), 1.0f}};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}

fp_t _minv3(mat3 m0, mat3 m1){
	fp_t det;
	mat3 m, r;
	cpym3(m, m1);

	det  = m[0][0]*(m[1][1]*m[2][2] - m[1][2]*m[2][1]);
	det -= m[1][0]*(m[0][1]*m[2][2] - m[0][2]*m[2][1]);
    	det += m[2][0]*(m[0][1]*m[1][2] - m[0][2]*m[1][1]);
	if(!det) return 0;
	
	r[0][0] = ( m[1][1]*m[2][2] - m[1][2]*m[2][1])/det;
	r[0][1] = (-m[0][1]*m[2][2] + m[0][2]*m[2][1])/det;
	r[0][2] = ( m[0][1]*m[1][2] - m[0][2]*m[1][1])/det;
	r[1][0] = (-m[1][0]*m[2][2] + m[1][2]*m[2][0])/det;
	r[1][1] = ( m[0][0]*m[2][2] - m[0][2]*m[2][0])/det;
	r[1][2] = (-m[0][0]*m[1][2] + m[0][2]*m[1][0])/det;
	r[2][0] = ( m[1][0]*m[2][1] - m[1][1]*m[2][0])/det;
	r[2][1] = (-m[0][0]*m[2][1] + m[0][1]*m[2][0])/det;
	r[2][2] = ( m[0][0]*m[1][1] - m[0][1]*m[1][0])/det;
	
	cpym3(m0, r);
	return det;
}

fp_t _minv4(mat4 m0, mat4 m1){
	fp_t det;
	mat4 m, r;
	cpym4(m, m1);

	det  = m[0][0]*(m[1][1]*m[2][2]*m[3][3] + m[1][2]*m[2][3]*m[3][1] +
	                m[1][3]*m[2][1]*m[3][2] - m[1][3]*m[2][2]*m[3][1] -
	                m[1][2]*m[2][1]*m[3][3] - m[1][1]*m[2][3]*m[3][2]);
	det -= m[1][0]*(m[0][1]*m[2][2]*m[3][3] + m[0][2]*m[2][3]*m[3][1] +
	                m[0][3]*m[2][1]*m[3][2] - m[0][3]*m[2][2]*m[3][1] -
	                m[0][2]*m[2][1]*m[3][3] - m[0][1]*m[2][3]*m[3][2]);
    	det += m[2][0]*(m[0][1]*m[1][2]*m[3][3] + m[0][2]*m[1][3]*m[3][1] +
                    m[0][3]*m[1][1]*m[3][2] - m[0][3]*m[1][2]*m[3][1] -
                    m[0][2]*m[1][1]*m[3][3] - m[0][1]*m[1][3]*m[3][2]);
    	det -= m[3][0]*(m[0][1]*m[1][2]*m[2][3] + m[0][2]*m[1][3]*m[2][1] +
                    m[0][3]*m[1][1]*m[2][2] - m[0][3]*m[1][2]*m[2][1] -
                    m[0][2]*m[1][1]*m[2][3] - m[0][1]*m[1][3]*m[2][2]);
    	if(!det) return 0;

    	r[0][0] = ( m[1][1]*m[2][2]*m[3][3] + m[1][2]*m[2][3]*m[3][1] +
				m[1][3]*m[2][1]*m[3][2] - m[1][3]*m[2][2]*m[3][1] -
                m[1][2]*m[2][1]*m[3][3] - m[1][1]*m[2][3]*m[3][2])/det;
    	r[0][1] = (-m[0][1]*m[2][2]*m[3][3] - m[0][2]*m[2][3]*m[3][1] -
                m[0][3]*m[2][1]*m[3][2] + m[0][3]*m[2][2]*m[3][1] +
                m[0][2]*m[2][1]*m[3][3] + m[0][1]*m[2][3]*m[3][2])/det;
    	r[0][2] = ( m[0][1]*m[1][2]*m[3][3] + m[0][2]*m[1][3]*m[3][1] +
                m[0][3]*m[1][1]*m[3][2] - m[0][3]*m[1][2]*m[3][1] -
                m[0][2]*m[1][1]*m[3][3] - m[0][1]*m[1][3]*m[3][2])/det;
    	r[0][3] = (-m[0][1]*m[1][2]*m[2][3] - m[0][2]*m[1][3]*m[2][1] -
                m[0][3]*m[1][1]*m[2][2] + m[0][3]*m[1][2]*m[2][1] +
                m[0][2]*m[1][1]*m[2][3] + m[0][1]*m[1][3]*m[2][2])/det;
    	r[1][0] = (-m[1][0]*m[2][2]*m[3][3] - m[1][2]*m[2][3]*m[3][0] -
                m[1][3]*m[2][0]*m[3][2] + m[1][3]*m[2][2]*m[3][0] +
                m[1][2]*m[2][0]*m[3][3] + m[1][0]*m[2][3]*m[3][2])/det;
    	r[1][1] = ( m[0][0]*m[2][2]*m[3][3] + m[0][2]*m[2][3]*m[3][0] +
                m[0][3]*m[2][0]*m[3][2] - m[0][3]*m[2][2]*m[3][0] -
                m[0][2]*m[2][0]*m[3][3] - m[0][0]*m[2][3]*m[3][2])/det;
    	r[1][2] = (-m[0][0]*m[1][2]*m[3][3] - m[0][2]*m[1][3]*m[3][0] -
                m[0][3]*m[1][0]*m[3][2] + m[0][3]*m[1][2]*m[3][0] +
                m[0][2]*m[1][0]*m[3][3] + m[0][0]*m[1][3]*m[3][2])/det;
    	r[1][3] = ( m[0][0]*m[1][2]*m[2][3] + m[0][2]*m[1][3]*m[2][0] +
                m[0][3]*m[1][0]*m[2][2] - m[0][3]*m[1][2]*m[2][0] -
                m[0][2]*m[1][0]*m[2][3] - m[0][0]*m[1][3]*m[2][2])/det;
    	r[2][0] = ( m[1][0]*m[2][1]*m[3][3] + m[1][1]*m[2][3]*m[3][0] +
                m[1][3]*m[2][0]*m[3][1] - m[1][3]*m[2][1]*m[3][0] -
                m[1][1]*m[2][0]*m[3][3] - m[1][0]*m[2][3]*m[3][1])/det;
    	r[2][1] = (-m[0][0]*m[2][1]*m[3][3] - m[0][1]*m[2][3]*m[3][0] -
                m[0][3]*m[2][0]*m[3][1] + m[0][3]*m[2][1]*m[3][0] +
                m[0][1]*m[2][0]*m[3][3] + m[0][0]*m[2][3]*m[3][1])/det;
    	r[2][2] = ( m[0][0]*m[1][1]*m[3][3] + m[0][1]*m[1][3]*m[3][0] +
                m[0][3]*m[1][0]*m[3][1] - m[0][3]*m[1][1]*m[3][0] -
                m[0][1]*m[1][0]*m[3][3] - m[0][0]*m[1][3]*m[3][1])/det;
    	r[2][3] = (-m[0][0]*m[1][1]*m[2][3] - m[0][1]*m[1][3]*m[2][0] -
                m[0][3]*m[1][0]*m[2][1] + m[0][3]*m[1][1]*m[2][0] +
                m[0][1]*m[1][0]*m[2][3] + m[0][0]*m[1][3]*m[2][1])/det;
    	r[3][0] = (-m[1][0]*m[2][1]*m[3][2] - m[1][1]*m[2][2]*m[3][0] -
                m[1][2]*m[2][0]*m[3][1] + m[1][2]*m[2][1]*m[3][0] +
                m[1][1]*m[2][0]*m[3][2] + m[1][0]*m[2][2]*m[3][1])/det;
    	r[3][1] = ( m[0][0]*m[2][1]*m[3][2] + m[0][1]*m[2][2]*m[3][0] +
                m[0][2]*m[2][0]*m[3][1] - m[0][2]*m[2][1]*m[3][0] -
                m[0][1]*m[2][0]*m[3][2] - m[0][0]*m[2][2]*m[3][1])/det;
    	r[3][2] = (-m[0][0]*m[1][1]*m[3][2] - m[0][1]*m[1][2]*m[3][0] -
                m[0][2]*m[1][0]*m[3][1] + m[0][2]*m[1][1]*m[3][0] +
                m[0][1]*m[1][0]*m[3][2] + m[0][0]*m[1][2]*m[3][1])/det;
    	r[3][3] = ( m[0][0]*m[1][1]*m[2][2] + m[0][1]*m[1][2]*m[2][0] +
                m[0][2]*m[1][0]*m[2][1] - m[0][2]*m[1][1]*m[2][0] -
                m[0][1]*m[1][0]*m[2][2] - m[0][0]*m[1][2]*m[2][1])/det;

    	cpym4(m0, r);
	return det;
}




void _qrot(vec3 v0, fp64_t ang, vec3 u, vec3 v1){
	ang /= 2;
	vec3 v;
	cpyv3(v, u);
	fp_t s = cos(ang), r = sin(ang);
	fp_t t = v[x]*v[x] + v[y]*v[y] + v[z]*v[z];
	
	if(t != 1.0f){
		t = sqrt(t)/r;
		v[x] = v[x]/t;
		v[y] = v[y]/t;
		v[z] = v[z]/t;
	}else{
		v[x] = v[x]*r;
		v[y] = v[y]*r;
		v[z] = v[z]*r;
	}
	
	mat3 m = {
		{1-2*(v[y]*v[y] + v[z]*v[z]), 2*(v[x]*v[y] - s*v[z]), 2*(v[x]*v[z] + s*v[y])},
		{2*(v[x]*v[y] + s*v[z]), 1-2*(v[z]*v[z] + v[x]*v[x]), 2*(v[y]*v[z] - s*v[x])},
		{2*(v[x]*v[z] - s*v[y]), 2*(v[y]*v[z] + s*v[x]), 1-2*(v[x]*v[x] + v[y]*v[y])}
	};
	
	mulmv3(v0, m, v1);
}

void _qtom(fp_t m0[4][4], fp64_t ang, vec3 u, mat4 om){
	//if(!m0 && !om) return;
	
	ang /= 2;
	vec3 v;
	cpyv3(v, u);
	fp_t s = cos(ang), r = sin(ang);
	fp_t t = v[x]*v[x] + v[y]*v[y] + v[z]*v[z];
	
	if(t != 1.0f){
		t = sqrt(t)/r;
		v[x] = v[x]/t;
		v[y] = v[y]/t;
		v[z] = v[z]/t;
	}else{
		v[x] = v[x]*r;
		v[y] = v[y]*r;
		v[z] = v[z]*r;
	}
	
	mat4 m = {
		{1-2*(v[y]*v[y] + v[z]*v[z]), 2*(v[x]*v[y] - s*v[z]), 2*(v[x]*v[z] + s*v[y]), 0},
		{2*(v[x]*v[y] + s*v[z]), 1-2*(v[z]*v[z] + v[x]*v[x]), 2*(v[y]*v[z] - s*v[x]), 0},
		{2*(v[x]*v[z] - s*v[y]), 2*(v[y]*v[z] + s*v[x]), 1-2*(v[x]*v[x] + v[y]*v[y]), 0},
		{0, 0, 0, 1}
	};
	
	if(om)
		mulmm4(om, om, m);
	if(m0)
		cpym4(m0, m);
}


void _qslerp(quat q0, quat q1, quat q2, fp_t n){
	quat _q;
	fp_t k1, k2, c;
	
	c = dotq(q1, q2);
	
	if(c < 0){
		negq(_q, q2);
		c = -c;
	}
	
	if(c > 0.9999f){
		k1 = 1 - n;
		k2 = n;
	}
	else{
		fp_t s = 1 - c*c;
		fp_t w = atan2(s, c);
		
		k1 = sin((1-n)*w)/s;
		k2 = sin(n*w)/s;
	}
	
	q0[x] = q1[x]*k1 + _q[x]*k2;
	q0[y] = q1[y]*k1 + _q[y]*k2;
	q0[z] = q1[z]*k1 + _q[z]*k2;
	q0[q] = q1[q]*k1 + _q[q]*k2;
	
	/*
	_qdif(q, q1, q2);
	_qpwr(q, q, n);
	_qmul(q0, q, q1);*/
}

#undef x
#undef y
#undef z
#undef q
